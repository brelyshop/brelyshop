import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SHOP_ROUTES } from './common/constants/shop-routes.constant';

const routes: Routes = [
  {
    path: '',
    redirectTo: SHOP_ROUTES.SHOP.NAME,
    pathMatch: 'full',
  },
  {
    path: SHOP_ROUTES.SHOP.NAME,
    loadChildren: () =>
      import('./features/shop/shop.module').then((m) => m.ShopModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { paramsInheritanceStrategy: 'always' }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
