import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shop-home',
  templateUrl: './shop-home.component.html',
  styleUrls: ['./shop-home.component.sass'],
})
export class ShopHomeComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    console.log('HOME');
  }
}
