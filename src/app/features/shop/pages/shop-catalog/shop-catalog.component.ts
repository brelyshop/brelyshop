import { Component, OnDestroy, OnInit } from '@angular/core';
import { Product } from '../../../../common/models/shop.model';
import { of, Subscription } from 'rxjs';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ProductHttpService } from '../../../../shared/services/product/product-http.service';
import {Pagination} from "../../../../common/models/pagination.model";
import {SHOP_ROUTES} from "../../../../common/constants/shop-routes.constant";


const {
  SHOP: {
    CHILDREN: {
      CATALOG: {
        CHILDREN: {
          CATEGORY: {
            CHILDREN: {
              ALL: { NAME: ALL_PATH },
            },
          },
        },
      },
    },
  },
} = SHOP_ROUTES;

@Component({
  selector: 'app-shop-catalog',
  templateUrl: './shop-catalog.component.html',
  styleUrls: ['./shop-catalog.component.sass'],
})
export class ShopCatalogComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  products: Product[] = [];
  pagination: Pagination = {
    currentPage: 1,
    itemsPerPage: 10,
    totalItems: 0,
    totalPages: 10,
  };

  constructor(
    private route: ActivatedRoute,
    private productHttpService: ProductHttpService
  ) {}

  ngOnInit(): void {
    this.listenRouteChanges();
  }

  ngOnDestroy() {
    this.subscriptions.map((s$) => s$.unsubscribe());
  }

  private listenRouteChanges = () => {
    this.subscriptions.push(
      this.route.paramMap.subscribe(async (params: ParamMap) => {
        const id = params.get('id') || '';
        await this.getProducts(id);
      })
    );
  };

  private getProducts = async (categoryId: string) => {
    const res = categoryId !== ALL_PATH
      ? await this.productHttpService.getByCategory(categoryId)
      : await this.productHttpService.get().toPromise();

    this.products = res.data;
    this.pagination = res.pagination;
  };
}
