import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { ShopMainLayoutComponent } from './components/shop-main-layout/shop-main-layout.component';
import { ShopHomeComponent } from './pages/shop-home/shop-home.component';
import { ShopSideMenuLeftComponent } from './components/shop-side-menu-left/shop-side-menu-left.component';
import { ShopRoutingModule } from './shop-routing.module';
import { ShopHeaderComponent } from './components/shop-header/shop-header.component';
import { ShopWelcomeComponent } from './components/shop-welcome/shop-welcome.component';
import { ShopWelcomeCategoriesComponent } from './components/shop-welcome-categories/shop-welcome-categories.component';
import { ShopCallToActionComponent } from './components/shop-call-to-action/shop-call-to-action.component';
import { ShopNewArrivalsComponent } from './components/shop-new-arrivals/shop-new-arrivals.component';
import { ShopBrandsComponent } from './components/shop-brands/shop-brands.component';
import { ShopFooterComponent } from './components/shop-footer/shop-footer.component';
import { ShopCartComponent } from './components/shop-cart/shop-cart.component';
import { ShopCatalogComponent } from './pages/shop-catalog/shop-catalog.component';
import { ShopBreadcrumbComponent } from './components/shop-breadcrumb/shop-breadcrumb.component';
import { ShopFiltersComponent } from './components/shop-filters/shop-filters.component';
import { ShopProductDiscountComponent } from './components/shop-product-discount/shop-product-discount.component';
import { ShopProductPriceComponent } from './components/shop-product-price/shop-product-price.component';
import { ShopProductItemComponent } from './components/shop-product-item/shop-product-item.component';
import { ShopProductPreviewComponent } from './components/shop-product-preview/shop-product-preview.component';
import { ShopProductViewComponent } from './pages/shop-product-view/shop-product-view.component';

@NgModule({
  declarations: [
    ShopMainLayoutComponent,
    ShopHomeComponent,
    ShopSideMenuLeftComponent,
    ShopHeaderComponent,
    ShopWelcomeComponent,
    ShopWelcomeCategoriesComponent,
    ShopCallToActionComponent,
    ShopNewArrivalsComponent,
    ShopBrandsComponent,
    ShopFooterComponent,
    ShopCartComponent,
    ShopCatalogComponent,
    ShopBreadcrumbComponent,
    ShopFiltersComponent,
    ShopProductDiscountComponent,
    ShopProductPriceComponent,
    ShopProductItemComponent,
    ShopProductPreviewComponent,
    ShopProductViewComponent,
  ],
  imports: [CommonModule, SharedModule, ShopRoutingModule],
})
export class ShopModule {}
