import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShopMainLayoutComponent } from './components/shop-main-layout/shop-main-layout.component';
import { SHOP_ROUTES } from '../../common/constants/shop-routes.constant';
import { ShopHomeComponent } from './pages/shop-home/shop-home.component';
import { ShopCatalogComponent } from './pages/shop-catalog/shop-catalog.component';
import {ShopProductViewComponent} from "./pages/shop-product-view/shop-product-view.component";

const {
  SHOP: {
    CHILDREN: {
      HOME,
      CATALOG: {
        CHILDREN: {
          CATEGORY: {
            CHILDREN: { ALL },
            NAME: CATEGORY_NAME,
          },
          PRODUCT: {
            NAME: PRODUCT_NAME
          }
        },
        NAME: CATALOG_NAME,
      },
    },
  },
} = SHOP_ROUTES;

const routes: Routes = [
  {
    path: '',
    component: ShopMainLayoutComponent,
    children: [
      { path: '', redirectTo: HOME.NAME, pathMatch: 'full' },
      {
        path: HOME.NAME,
        component: ShopHomeComponent,
      },
      {
        path: CATALOG_NAME,
        children: [
          {
            path: '',
            redirectTo: `${CATEGORY_NAME}/${ALL.NAME}`,
            pathMatch: 'full',
          },
          {
            path: `${CATEGORY_NAME}/:id`,
            component: ShopCatalogComponent,
          },
          {
            path: `${PRODUCT_NAME}/:id`,
            component: ShopProductViewComponent
          }
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShopRoutingModule {}
