import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopCallToActionComponent } from './shop-call-to-action.component';

describe('ShopCallToActionComponent', () => {
  let component: ShopCallToActionComponent;
  let fixture: ComponentFixture<ShopCallToActionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ShopCallToActionComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopCallToActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
