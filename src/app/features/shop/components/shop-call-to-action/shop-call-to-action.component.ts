import { Component, OnInit } from '@angular/core';
import {SHOP_ROUTES} from "../../../../common/constants/shop-routes.constant";
const {
  SHOP: {
    NAME: SHOP_PATH,
    CHILDREN: {
      CATALOG: {
        NAME: CATALOG_PATH,
        CHILDREN: {
          CATEGORY: {
            NAME: CATEGORY_PATH,
            CHILDREN: {
              ALL: { NAME: ALL_PATH },
            },
          },
        },
      },
    },
  },
} = SHOP_ROUTES;

@Component({
  selector: 'app-shop-call-to-action',
  templateUrl: './shop-call-to-action.component.html',
  styleUrls: ['./shop-call-to-action.component.sass'],
})
export class ShopCallToActionComponent implements OnInit {
  catalogPath: string = '';

  constructor() {}

  ngOnInit(): void {
    this.catalogPath = `/${SHOP_PATH}/${CATALOG_PATH}/${CATEGORY_PATH}/${ALL_PATH}`;
  }
}
