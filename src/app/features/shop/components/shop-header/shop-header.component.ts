import { Component, OnInit } from '@angular/core';
import { SHOP_ROUTES } from '../../../../common/constants/shop-routes.constant';

const {
  SHOP: {
    CHILDREN: { CATALOG, HOME },
  },
} = SHOP_ROUTES;

@Component({
  selector: 'app-shop-header',
  templateUrl: './shop-header.component.html',
  styleUrls: ['./shop-header.component.sass'],
})
export class ShopHeaderComponent implements OnInit {
  menu: Menu[] = [
    {
      name: 'Catalogo',
      path: CATALOG.NAME,
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}

interface Menu {
  name: string;
  path: string;
}
