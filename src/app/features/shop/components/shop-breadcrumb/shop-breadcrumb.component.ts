import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Category } from '../../../../common/models/shop.model';
import { CategoryHttpsService } from '../../../../shared/services/category/category-https.service';

@Component({
  selector: 'app-shop-breadcrumb',
  templateUrl: './shop-breadcrumb.component.html',
  styleUrls: ['./shop-breadcrumb.component.sass'],
})
export class ShopBreadcrumbComponent implements OnInit, OnDestroy {
  // @ts-ignore
  private routeSubscription: Subscription = null;
  // @ts-ignore
  currentCategory: Category = null;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private categoryHttpService: CategoryHttpsService
  ) {}

  ngOnInit(): void {
    this.listenRouteChanges();
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  private listenRouteChanges = () => {
    this.routeSubscription = this.route.paramMap.subscribe(
      (params: ParamMap) => {
        const id = params.get('id') || '';
        this.getCategory(id);
      }
    );
  };

  private getCategory = async (id: string) => {
    this.currentCategory = await this.categoryHttpService.find(id).toPromise();
  };
}
