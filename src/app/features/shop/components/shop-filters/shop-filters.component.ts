import { Component, OnInit } from '@angular/core';
import { CategoryHttpsService } from '../../../../shared/services/category/category-https.service';
import { Category } from '../../../../common/models/shop.model';
import { SHOP_ROUTES } from '../../../../common/constants/shop-routes.constant';

const {
  SHOP: {
    NAME: SHOP_NAME,
    CHILDREN: {
      CATALOG: {
        CHILDREN: { CATEGORY },
        NAME: CATALOG_NAME,
      },
    },
  },
} = SHOP_ROUTES;

@Component({
  selector: 'app-shop-filters',
  templateUrl: './shop-filters.component.html',
  styleUrls: ['./shop-filters.component.sass'],
})
export class ShopFiltersComponent implements OnInit {
  categoriesPath: string = `/${SHOP_NAME}/${CATALOG_NAME}/${CATEGORY.NAME}`;
  categories: Category[] = [];

  constructor(private categoriesHttpService: CategoryHttpsService) {}

  ngOnInit(): void {
    this.getCategories();
  }

  private getCategories = async () => {
    this.categories = await this.categoriesHttpService.get().toPromise();
  };
}
