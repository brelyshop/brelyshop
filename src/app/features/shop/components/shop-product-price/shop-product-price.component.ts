import { Component, Input, OnInit } from '@angular/core';
import { Price, PriceType } from '../../../../common/models/shop.model';

@Component({
  selector: 'app-shop-product-price',
  templateUrl: './shop-product-price.component.html',
  styleUrls: ['./shop-product-price.component.sass'],
})
export class ShopProductPriceComponent implements OnInit {
  @Input() prices: Price[] = [];
  oldPrice: number = 0;
  currentPrice: number = 0;

  constructor() {}

  ngOnInit(): void {
    this.calculatePercentage();
  }

  private calculatePercentage = () => {
    try {
      // @ts-ignore
      const { value: currentPrice } = this.prices.find(
        (p) => p.type === PriceType.Current
      );
      this.currentPrice = currentPrice;

      // @ts-ignore
      const { value: oldPrice } = this.prices.find(
        (p) => p.type === PriceType.Old
      );
      // @ts-ignore

      if (!oldPrice) {
        throw new Error();
      }
      this.oldPrice = oldPrice;
    } catch (e) {
      console.log(e);
    }
  };
}
