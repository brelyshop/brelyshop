import { Component, Input, OnInit } from '@angular/core';
import { Price, PriceType } from '../../../../common/models/shop.model';

@Component({
  selector: 'app-shop-product-discount',
  templateUrl: './shop-product-discount.component.html',
  styleUrls: ['./shop-product-discount.component.sass'],
})
export class ShopProductDiscountComponent implements OnInit {
  @Input() prices: Price[] = [];
  discountPercentage: number = 0;

  constructor() {}

  ngOnInit(): void {
    this.calculatePercentage();
  }

  private calculatePercentage = () => {
    try {
      // @ts-ignore
      const { value: oldPrice } = this.prices.find(
        (p) => p.type === PriceType.Old
      );
      // @ts-ignore
      const { value: currentPrice } = this.prices.find(
        (p) => p.type === PriceType.Current
      );
      if (!oldPrice) {
        throw new Error();
      }
      this.discountPercentage = (currentPrice * 100) / oldPrice;
    } catch (e) {
      console.log(e);
    }
  };
}
