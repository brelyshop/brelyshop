import { Component, OnInit } from '@angular/core';
import { Category } from '../../../../common/models/shop.model';
import { CategoryHttpsService } from '../../../../shared/services/category/category-https.service';
import {SHOP_ROUTES} from "../../../../common/constants/shop-routes.constant";
import {Router} from "@angular/router";
const {
  SHOP: {
    NAME: SHOP_PATH,
    CHILDREN: {
      CATALOG: {
        NAME: CATALOG_PATH,
        CHILDREN: {
          CATEGORY: {
            NAME: CATEGORY_PATH,
            CHILDREN: {
              ALL: { NAME: ALL_PATH },
            },
          },
        },
      },
    },
  },
} = SHOP_ROUTES;
@Component({
  selector: 'app-shop-welcome-categories',
  templateUrl: './shop-welcome-categories.component.html',
  styleUrls: ['./shop-welcome-categories.component.sass'],
})
export class ShopWelcomeCategoriesComponent implements OnInit {
  categories: Category[] = [];
  catalogPath: string = '';

  constructor(private categoriesHttpService: CategoryHttpsService, private router: Router) {}

  ngOnInit(): void {
    this.getCategories();
  }

  private getCategories = async () => {
    this.categories = await this.categoriesHttpService.get(3).toPromise();
    this.catalogPath = `/${SHOP_PATH}/${CATALOG_PATH}/${CATEGORY_PATH}/`;
  };

  go = (category: string) => {
    this.router.navigate([`${this.catalogPath}${category}`])
  }
}
