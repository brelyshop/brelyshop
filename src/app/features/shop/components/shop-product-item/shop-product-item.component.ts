import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import { Product } from '../../../../common/models/shop.model';
import {SHOP_ROUTES} from "../../../../common/constants/shop-routes.constant";
import {Router} from "@angular/router";
const {
  SHOP: {
    NAME: SHOP_PATH,
    CHILDREN: {
      CATALOG: {
        NAME: CATALOG_PATH,
        CHILDREN: {
          PRODUCT: {
            NAME: PRODUCT_PATH
          }
        },
      },
    },
  },
} = SHOP_ROUTES;
@Component({
  selector: 'app-shop-product-item',
  templateUrl: './shop-product-item.component.html',
  styleUrls: ['./shop-product-item.component.sass'],
})
export class ShopProductItemComponent implements OnInit, OnChanges {
  // @ts-ignore
  @Input() product: Product = null;
  inventoryQty = 0
  productViewPath: string = ''

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.productViewPath = `/${SHOP_PATH}/${CATALOG_PATH}/${PRODUCT_PATH}/`;
  }

  ngOnChanges({product}: SimpleChanges) {
    if (product) {
      try {
        this.product.models?.forEach(model => this.inventoryQty += model.quantity)
      } catch (e) {}
    }
  }

  goToProduct = () => {
    this.router.navigate([`${this.productViewPath}${this.product.id}`])
  }
}
