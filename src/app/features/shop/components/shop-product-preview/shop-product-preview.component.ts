import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../../../../common/models/shop.model';

@Component({
  selector: 'app-shop-product-preview',
  templateUrl: './shop-product-preview.component.html',
  styleUrls: ['./shop-product-preview.component.sass'],
})
export class ShopProductPreviewComponent implements OnInit {
  // @ts-ignore
  @Input() product: Product = null;
  image1: string = '';
  image2: string = '';
  images: string[] = [];

  constructor() {}

  ngOnInit(): void {
    this.getImages();
  }

  private getImages = () => {
    try {
      const { models } = this.product;
      // @ts-ignore
      for (const { images } of models) {
        // @ts-ignore
        this.images = [...this.images, ...images];
      }
      this.image1 = this.images[0];
      this.image2 = this.images[1];
    } catch (e) {
      this.image1 = './assets/img/gallery.jpeg';
    }
  };
}
