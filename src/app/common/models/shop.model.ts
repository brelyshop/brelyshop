export interface Category {
  id: string;
  name: string;
  parent_id?: string;
  image_preview?: string;
}

export interface Product {
  id: string;
  name: string;
  category_id: string;
  tags?: string[];
  models?: Model[];
  prices: Price[];
}

export interface Model {
  id: string;
  name: string;
  product_id: string;
  details?: Details[];
  images?: string[];
  quantity: number;
}

export interface Details {
  id: string;
  name: string;
  colors: Color[];
}

export interface Color {
  id: string;
  name: string;
  hex: string;
}

export interface Price {
  type: PriceType;
  value: number;
}

export enum PriceType {
  Old = 'old',
  Current = 'current',
}
