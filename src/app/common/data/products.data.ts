import { PriceType, Product } from '../models/shop.model';

const BAGS: Product[] = [
  {
    id: '840a5c7e-53a5-11ec-bf63-0242ac130002',
    name: 'BOLSA 100% PIEL TRES COLORES',
    category_id: 'bolsas',
    models: [
      {
        id: '840a5c7e-53a5-11ec-bf63-0242ac130002',
        name: 'Modelo 1',
        product_id: '01',
        images: [
          '3a544a6e-24ff-4de3-9783-26e76574fc65.jpeg',
        ],
        quantity: 0,
      },
    ],
    prices: [
      {
        type: PriceType.Old,
        value: 300,
      },
      {
        type: PriceType.Current,
        value: 150,
      },
    ],
  },
  {
    id: 'ecd68975-db2c-4f44-8301-faf875ae59e5',
    name: 'BOLSA 100% PIEL CUADRADA',
    category_id: 'bolsas',
    models: [
      {
        id: 'ecd68975-db2c-4f44-8301-faf875ae59e5',
        name: 'Modelo 1',
        product_id: '02',
        images: [
          'e4d72a82-11c7-4eb2-bc56-1ee2a704cdc0.jpeg',
        ],
        quantity: 0,
      },
    ],
    prices: [
      {
        type: PriceType.Old,
        value: 300,
      },
      {
        type: PriceType.Current,
        value: 150,
      },
    ],
  },
  {
    id: 'fa9225b9-b740-4443-8a4e-a7fc76630969',
    name: 'BOLSA 100% PIEL TIPO MARICONERA',
    category_id: 'bolsas',
    models: [
      {
        id: 'fa9225b9-b740-4443-8a4e-a7fc76630969',
        name: 'Modelo 1',
        product_id: '03',
        images: [
          'e7466ae8-7c80-4272-aef1-b6b27c05bfc1.jpeg',
        ],
        quantity: 3,
      },
    ],
    prices: [
      {
        type: PriceType.Old,
        value: 300,
      },
      {
        type: PriceType.Current,
        value: 150,
      },
    ],
  },
  {
    id: '4aca8322-c2c3-43a0-9ac3-510d82ee08b9',
    name: 'MARICONERA 100% PIEL',
    category_id: 'bolsas',
    models: [
      {
        id: '0f5a0af4-8098-4d17-a5da-7ce7259e152a',
        name: 'Modelo 1',
        product_id: '4aca8322-c2c3-43a0-9ac3-510d82ee08b9',
        images: [
          'e99cab36-5fe9-46ce-be88-d2a94819fe14.jpeg',
        ],
        quantity: 1,
      },
    ],
    prices: [
      {
        type: PriceType.Old,
        value: 300,
      },
      {
        type: PriceType.Current,
        value: 150,
      },
    ],
  }


]

const WALLETS: Product[] = [
  {
    id: '8807032b-5f83-4b75-941d-3dee67da2c2a',
    name: 'CARTERA EFECTO REFLEJANTE',
    category_id: 'carteras',
    models: [
      {
        id: '8807032b-5f83-4b75-941d-3dee67da2c2a',
        name: 'Modelo 1',
        product_id: '04',
        images: [
          'ae685041-9b87-47f4-a200-b4bf51d22a4d.jpeg',
        ],
        quantity: 1,
      },
    ],
    prices: [
      {
        type: PriceType.Old,
        value: 300,
      },
      {
        type: PriceType.Current,
        value: 150,
      },
    ],
  },
]

const COSMETIC_BAGS: Product[] = [
  {
    id: 'd0f4889f-08bf-463a-9d3c-29b523b9172c',
    name: 'COSMETIQUERA EFECTO REFLEJANTE',
    category_id: 'cosmetiqueras',
    models: [
      {
        id: '05m01',
        name: 'Modelo 1',
        product_id: 'd0f4889f-08bf-463a-9d3c-29b523b9172c',
        images: [
          'cb647c1e-7031-47f8-bbb1-ae891afc19c5.jpeg',
        ],
        quantity: 1,
      },
      {
        id: '05m02',
        name: 'Modelo 2',
        product_id: 'd0f4889f-08bf-463a-9d3c-29b523b9172c',
        images: [
          '10ee5433-2a67-429d-a8ca-65651e830140.jpeg',
        ],
        quantity: 1,
      },
      {
        id: '05m03',
        name: 'Modelo 3',
        product_id: 'd0f4889f-08bf-463a-9d3c-29b523b9172c',
        images: [
          'aabb0c53-8e37-4812-adc0-3dfda1f1d298.jpeg',
        ],
        quantity: 1,
      },
      {
        id: '05m04',
        name: 'Modelo 4',
        product_id: 'd0f4889f-08bf-463a-9d3c-29b523b9172c',
        images: [
          'c8537522-228f-4032-b7f0-1487f7e47c3f.jpeg',
        ],
        quantity: 1,
      },
      {
        id: '05m05',
        name: 'Modelo 5',
        product_id: 'd0f4889f-08bf-463a-9d3c-29b523b9172c',
        images: [
          'ff43ec4c-547e-4f83-a6c1-591798d86936.jpeg',
        ],
        quantity: 1,
      },
    ],
    prices: [
      {
        type: PriceType.Old,
        value: 300,
      },
      {
        type: PriceType.Current,
        value: 150,
      },
    ],
  },
]

const CAPS: Product[] = [
  {
    id: 'e4daade1-a9cc-4cb1-a237-021775450a99',
    name: 'GORRA 100% PIEL',
    category_id: 'gorras',
    models: [
      {
        id: '06m01',
        name: 'Modelo 1',
        product_id: 'd0f4889f-08bf-463a-9d3c-29b523b9172c',
        images: [
          '8c95e4ee-f34b-44aa-bb85-207569c2e20f.jpeg',
        ],
        quantity: 1,
      },
      {
        id: '06m02',
        name: 'Modelo 2',
        product_id: 'd0f4889f-08bf-463a-9d3c-29b523b9172c',
        images: [
          '69e58f9a-aa95-4d9e-93c1-6857e28ddb76.jpeg',
        ],
        quantity: 1,
      },
      {
        id: '06m03',
        name: 'Modelo 3',
        product_id: 'd0f4889f-08bf-463a-9d3c-29b523b9172c',
        images: [
          '8938d887-512c-4e59-aac6-7e42d315dba2.jpeg',
        ],
        quantity: 1,
      },
      {
        id: '07m04',
        name: 'Modelo 4',
        product_id: 'd0f4889f-08bf-463a-9d3c-29b523b9172c',
        images: [
          'cf18db12-c5e6-4ca6-a0ab-f9c66d1df27c.jpeg',
        ],
        quantity: 1,
      },
    ],
    prices: [
      {
        type: PriceType.Old,
        value: 300,
      },
      {
        type: PriceType.Current,
        value: 150,
      },
    ],
  },
]

const FLOWERPOTS: Product[] = [
  {
    id: 'a35a0e98-ebd3-4917-9063-0bdad6376c51',
    name: 'MACETA DE BARRO',
    category_id: 'macetas',
    models: [
      {
        id: '05m01',
        name: 'Maceta blanca',
        product_id: 'd0f4889f-08bf-463a-9d3c-29b523b9172c',
        images: [
          '0de46a03-da6c-4977-ad49-49c47ac8e61f.jpeg',
        ],
        quantity: 1,
      },
      {
        id: '05m02',
        name: 'Maceta blanca',
        product_id: 'd0f4889f-08bf-463a-9d3c-29b523b9172c',
        images: [
          '5d77af11-9213-4290-92b8-ea073e4da640.jpeg',
          '04112e02-ae72-4ea0-b3bb-f7c779176931.jpeg',
          'd73f24da-1d67-4a15-b56f-39cb7854abdc.jpeg'
        ],
        quantity: 1,
      },
      {
        id: '05m03',
        name: 'Maceta dorada',
        product_id: 'd0f4889f-08bf-463a-9d3c-29b523b9172c',
        images: [
          'af113f0a-67d3-4bfa-b502-82464d67c90c.jpeg',
          'b296a5fc-5345-498f-8773-33af5936dce4.jpeg'
        ],
        quantity: 1,
      },
    ],
    prices: [
      {
        type: PriceType.Old,
        value: 300,
      },
      {
        type: PriceType.Current,
        value: 150,
      },
    ],
  },
]


export const PRODUCTS: Product[] = [
  ...BAGS,
  ...WALLETS,
  ...COSMETIC_BAGS,
  ...CAPS,
  ...FLOWERPOTS,
  ...[]
  // {
  //   id: 'P1',
  //   name: 'Bolsa tipo 1',
  //   category_id: 'bolsas',
  //   prices: [
  //     {
  //       type: PriceType.Old,
  //       value: 300,
  //     },
  //     {
  //       type: PriceType.Current,
  //       value: 150,
  //     },
  //   ],
  //   models: [
  //     {
  //       id: 'model1',
  //       name: 'Modelo 1',
  //       product_id: 'P1',
  //       images: [
  //         'https://cdn.shopify.com/s/files/1/1669/1671/files/maleta-hecha-en-piel-camuflaje_480x480.jpg?v=1619115617',
  //         'https://ss511.liverpool.com.mx/xl/1095388465.jpg',
  //         'https://www.fridamexico.com/wp-content/uploads/2019/08/Regalos-Corporativos-Artesanales-Frida-Mexico-Bolsa-de-Piel-Huichol-Marca-MAIXIC-Modelo-Everest002.jpg',
  //       ],
  //       quantity: 10,
  //     },
  //     {
  //       id: 'model2',
  //       name: 'Modelo 2',
  //       product_id: 'P2',
  //       images: [
  //         'https://agleather.mx/wp-content/uploads/flapnegro-cm-600x600.jpg',
  //       ],
  //       quantity: 10,
  //     },
  //   ],
  // },
  // {
  //   id: 'P2',
  //   name: 'Bolsa tipo 2',
  //   category_id: 'bolsas',
  //   prices: [
  //     {
  //       type: PriceType.Old,
  //       value: 800,
  //     },
  //     {
  //       type: PriceType.Current,
  //       value: 999,
  //     },
  //   ],
  // },
  // {
  //   id: 'M1',
  //   name: 'Maceta tipo 1',
  //   category_id: 'macetas',
  //   prices: [
  //     {
  //       type: PriceType.Current,
  //       value: 150,
  //     },
  //   ],
  // },
];

