import { Category } from '../models/shop.model';

export const CATEGORIES: Category[] = [
  {
    id: 'bolsas',
    name: 'Bolsas',
    image_preview: 'https://cdn2.melodijolola.com/media/files/styles/nota_imagen/public/field/image/bolsa1.jpg'
  },
  {
    id: 'carteras',
    name: 'Carteras',
    image_preview: 'https://pieldeleon.com.mx/wp-content/uploads/2018/10/cartera-piel-hombre-cafe-melq-exterior.jpg'
  },
  {
    id: 'cosmetiqueras',
    name: 'Cosmetiqueras',
    image_preview: 'https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/79642272175.jpg'
  },
  {
    id: 'gorras',
    name: 'Gorras',
    image_preview: 'https://m.media-amazon.com/images/I/51XoZOT4Z4L._AC_SL1000_.jpg'
  },
  {
    id: 'macetas',
    name: 'Macetas',
    parent_id: 'hogar',
    image_preview: 'https://instalartodo.com/wp-content/uploads/2019/05/MacetasCemento4.jpg'
  },
  {
    id: 'hogar',
    name: 'Hogar',
    image_preview: 'https://instalartodo.com/wp-content/uploads/2019/05/MacetasCemento4.jpg'
  },
];
