export const SHOP_ROUTES = {
  SHOP: {
    NAME: 'tienda',
    CHILDREN: {
      HOME: {
        NAME: 'inicio',
      },
      CATALOG: {
        NAME: 'catalogo',
        CHILDREN: {
          CATEGORY: {
            NAME: 'categoria',
            CHILDREN: {
              ALL: {
                NAME: 'todo',
              },
            },
          },
          PRODUCT: {
            NAME: 'producto'
          }
        },
      },
    },
  },
};
