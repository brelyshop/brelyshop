import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { PRODUCTS } from '../../../common/data/products.data';
import {Pagination} from "../../../common/models/pagination.model";
import {Category, Product} from "../../../common/models/shop.model";
import {CategoryHttpsService} from "../category/category-https.service";
import {CATEGORIES} from "../../../common/data/categories.data";

@Injectable({
  providedIn: 'root',
})
export class ProductHttpService {
  constructor(private categoryHttpService: CategoryHttpsService) {}

  public getByCategory = async (categoryId: string): Promise<any> => {
    let items = PRODUCTS.filter(({category_id}) => category_id === categoryId)

    if (!items.length) {
      items = await this.getByParentCategory(categoryId)
      return Promise.resolve(
        {data: items, pagination: this.getPagination(items)}
      );
    } else {
      return Promise.resolve({data: items, pagination: this.getPagination(items)})
    }
  };

  private getByParentCategory = async (categoryId: string): Promise<Product[]> => {
    const categories = CATEGORIES.filter(({parent_id}) => parent_id === categoryId);
      let products: Product[] = []

    for(const cat of categories) {
        const prods: Product[] = PRODUCTS.filter(({category_id}) => category_id === cat.id)
        products = [...products, ...prods]
    }
    return products
  }

  public get = (): Observable<any> => {
    return of({data: PRODUCTS, pagination: this.getPagination(PRODUCTS)});
  };

  // TODO: remove
  private getPagination = (items: any): Pagination => {
    return {itemsPerPage: items.length, totalItems: items.length, currentPage: 1, totalPages: this.getTotalPages(items, items.length)};
  };
  private getTotalPages = (items: any, itemsPerPage: number): number => {
    return Math.ceil(items/itemsPerPage);

  }
}
