import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { CATEGORIES } from '../../../common/data/categories.data';

@Injectable({
  providedIn: 'root',
})
export class CategoryHttpsService {
  constructor() {}

  public get = (itemsPerPage?: number): Observable<any> => {
    if (itemsPerPage) {
      return of(CATEGORIES.slice(0, itemsPerPage));
    }
    return of(CATEGORIES);
  };

  public find = (id: string): Observable<any> => {
    return of(CATEGORIES.find((cat) => cat.id === id));
  };

  public getSubcategories = (parentId: string, itemsPerPage?: number): Observable<any> => {
    if (itemsPerPage) {
      return of(CATEGORIES.filter(({parent_id}) => parent_id === parentId).slice(0, itemsPerPage));
    }
    return of(CATEGORIES.filter(({parent_id}) => parent_id === parentId));
  };
}
